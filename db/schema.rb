# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_11_15_213015) do

  create_table "bills", force: :cascade do |t|
    t.integer "category_id"
    t.string "label", null: false
    t.decimal "price", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["category_id"], name: "index_bills_on_category_id"
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "payeds", force: :cascade do |t|
    t.integer "bill_id", null: false
    t.integer "payer_id", null: false
    t.datetime "payedAt"
    t.decimal "claim"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["bill_id"], name: "index_payeds_on_bill_id"
    t.index ["payer_id"], name: "index_payeds_on_payer_id"
  end

  create_table "payers", force: :cascade do |t|
    t.string "name"
    t.decimal "payRatio"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  add_foreign_key "bills", "categories"
  add_foreign_key "payeds", "bills"
  add_foreign_key "payeds", "payers"
end
