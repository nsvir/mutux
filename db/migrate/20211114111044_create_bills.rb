class CreateBills < ActiveRecord::Migration[6.1]
  def change
    create_table :bills do |t|
      t.references :category, null: true, foreign_key: true
      t.string :label, :null => false
      t.decimal :price, :null => false

      t.timestamps
    end
  end
end
