class CreatePayeds < ActiveRecord::Migration[6.1]
  def change
    create_table :payeds do |t|
      t.references :bill, null: false, foreign_key: true
      t.references :payer, null: false, foreign_key: true
      t.timestamp :payedAt
      t.decimal :claim

      t.timestamps
    end
  end
end
