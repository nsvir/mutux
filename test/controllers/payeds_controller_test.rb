require "test_helper"

class PayedsControllerTest < ActionDispatch::IntegrationTest
  fixtures :categories, :payers

  test "should get index" do
    get payed_index_path
    assert_response :success
  end

  test "should post create and set claim to half the price" do
    assert Payer.first.payRatio == 0.5, "payers must have payRatio to 0.5"

    post payed_index_path, params: {
      "label": "example",
      "price": 30,
      "category": Category.first.id,
      "payer": Payer.first.id,
      "payedAt": "2020-12-15T01:00:00"
    }

    assert_response :success
    assert_equal "15.0", JSON.parse(response.body)["claim"]
  end

  test "should update claim" do
    assert Payer.first.payRatio == 0.5, "payers must have payRatio to 0.5"

    put payed_path(Payed.first.id), params: {
      "price": 14
    }

    assert_response :success
    assert_equal "7.0", JSON.parse(response.body)["claim"]
  end

  test "Failed in payed should not create a bill" do
    assert_no_difference "Bill.all.size" do

      post payed_index_path, params: {
        "label": "example",
        "price": 30,
        "category": Category.first.id,
        "payer": -1, # Not found payer
        "payedAt": "2020-12-15T01:00:00"
      }
      assert_response :missing

    end
  end

  test "Delete a payed removes the associated bill" do
    assert_difference "Bill.count", -1 do
      assert_difference "Payed.count", -1 do
        delete payed_path(Payed.first.id)
        assert_response :success
      end
    end
  end

  test "Delete missing payed" do
    delete payed_path(-1)
    assert_response :missing
  end

end
