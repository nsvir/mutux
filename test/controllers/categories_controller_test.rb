require "test_helper"

class CategoriesControllerTest < ActionDispatch::IntegrationTest

  test "should get get" do
    get categories_path
    assert_response :success
  end

  test "should get show" do
    category = Category.create({ name: "test" })

    get category_path(category.id)
    assert_response :success
    assert_equal "test", JSON.parse(response.body)["name"]
  end

  test "should post create" do
    post categories_path, params: { name: "test" }
    
    assert_response :success
    assert_equal "test", JSON.parse(response.body)["name"]
  end

  test "should put update" do
    category = Category.create(name: "test")
    
    put category_path(category.id), params: { name: "newName"}
    assert_response :success
    assert_equal "newName", JSON.parse(response.body)["name"]
  end

  test "should delete destroy" do
    category = Category.create(name: "test")

    delete category_path(category.id)
    assert_response :success
  end
end
