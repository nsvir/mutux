require "test_helper"

class CategoryTest < ActiveSupport::TestCase

  test "category deletion set bill category to null" do

    assert_no_difference "Bill.count" do
      Category.destroy(Bill.first.category.id)
    end

  end

end