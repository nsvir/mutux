require "test_helper"

class PayerTest < ActiveSupport::TestCase

  def setup
    assert Payer.count == 1, "only one payer should be defined"
  end

  test "no more payer allowed" do
    Payer.create!(payRatio: 0.5)

    payer = Payer.new(payRatio: 0.5)
    assert_not payer.save
  end

  test "valid payRatio" do
    payer = Payer.new(payRatio: 0.5)
    assert payer.save
  end

  test "invalid payRatio nil" do
    payer = Payer.new
    assert_not payer.save
  end

  test "invalid payRatio greater than 1" do
    payer = Payer.new(payRatio: 2)
    assert_not payer.save
  end

  test "invalid payRatio lower than 1" do
    payer = Payer.new(payRatio: -1)
    assert_not payer.save
  end

  test "after update, recompute the ratios" do
    first = Payer.first
    second = Payer.create!(name: "second", payRatio: 0.5)

    first.update(payRatio: 0.2)

    assert_equal 0.8, second.reload.payRatio
  end
  
end
