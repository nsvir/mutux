require "test_helper"

class PayedTest < ActiveSupport::TestCase
  
  def setup
    @payer = Payer.new({ name: "test", payRatio: 0.5})
    @category = Category.new({ name: "test"})
    @bill = Bill.new({ label: "label", price: 30, category: @category})
    @payed = Payed.new({
      payer: @payer, bill: @bill, claim: 9.99, payedAt: Time.now
    })
  end
  
  test "valid payed" do
    assert @payed.valid?
  end
  
  test "payed with nil payer is ko" do
    @payed.payer = nil
    refute @payed.valid?, 'saved payed without a payer'
    assert_not_nil @payed.errors[:payer], 'no validation error for payer present'
  end

  test "payed with nil bill is ko" do
    @payed.bill = nil
    refute @payed.valid?, 'saved payed without a bill'
    assert_not_nil @payed.errors[:bill], 'no validation error for payer present'
  end

  test "payed with nil claim is ko" do
    @payed.claim = nil
    refute @payed.valid?, 'saved payed without a claim'
    assert_not_nil @payed.errors[:claim], 'no validation error for payer present'
  end

  test "payed with nil payed_at is ko" do
    @payed.payedAt = nil
    refute @payed.valid?, 'saved payed without a payed_at'
    assert_not_nil @payed.errors[:payed_at], 'no validation error for payer present'
  end

end
