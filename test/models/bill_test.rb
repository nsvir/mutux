require "test_helper"

class BillTest < ActiveSupport::TestCase

  def setup
    @category = Category.new({name: "name"})
    @bill = Bill.new(label: "label", price: 30, category: @category)
  end
  
  test "valid bill" do
    assert @bill.valid?
  end
  
  test "creating bill with nil category is ko" do
    @bill.category = nil
    refute @bill.valid?, 'saved bill without a category'
    assert_not_nil @bill.errors[:category], 'no validation error for category present'
  end

  test "update bill.category to null ok" do
    @bill.save()
    @bill.update(category: nil)

    assert @bill.reload.category == nil
  end

end
