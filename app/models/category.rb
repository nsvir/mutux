class Category < ApplicationRecord

    before_destroy :billsNullReference

    private 

    def billsNullReference
        Bill.where(category_id: self.id)
            .update_all(category_id: nil)
    end

end
