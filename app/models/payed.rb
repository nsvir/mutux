class Payed < ApplicationRecord
  belongs_to :bill, dependent: :destroy
  belongs_to :payer
  validates :claim, :payedAt, presence: true
end
