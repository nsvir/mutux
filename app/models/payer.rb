class NoMoreThan2 < ActiveModel::Validator
    def validate(record)
        if Payer.all.length >= 2
            record.errors.add :base, "Already 2 payers are defined"
        end
    end
end


class Payer < ApplicationRecord

    validates :payRatio, numericality: { less_than_or_equal_to: 1, greater_than_or_equal_to: 0 }
    validates_with NoMoreThan2, on: :create

    after_update :updateRatio

    def updateRatio
        other = Payer.where("id != #{id}").first

        # update_column is used to bypass callback to prevent infinite loop
        other.update_column(:payRatio, 1 - payRatio)
        other.update_column(:updated_at, updated_at)        
    end

end
