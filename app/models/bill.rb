class Bill < ApplicationRecord
  belongs_to :category, optional: true

  validates :price, :label, presence: true
  validates :category, presence: true, on: :create
end
