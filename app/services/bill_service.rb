class BillService

  def create(params)
    Bill.create!(with_category(params))
  end

  def update(bill, params)
    bill.update!(with_category(params))
  end

  private

  def with_category(params)
    result = params.clone
    result["category"] = Category.find(params["category"]) unless params["category"].nil?
    result
  end

end

