class PayedService

  def create(bill, params)
    Payed.create!(with_payer_and_claim(params, bill, params["payer"]))
  end

  def update(payed, bill, params)
    payerId = params["payer"].nil? ? payed.payer.id : params["payer"]
    payed.update!(with_payer_and_claim(params, bill, payerId))
  end

  private

  def with_ratio(price, ratio)
    price * ratio
  end

  def with_payer_and_claim(params, bill, payer)
    result = params.clone
    result["payer"] = Payer.find(payer)
    result["claim"] = with_ratio(bill.price,  result["payer"].payRatio)
    result["bill"] = bill
    result
  end

end
