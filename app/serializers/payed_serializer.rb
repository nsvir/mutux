class PayedSerializer < ActiveModel::Serializer
  attributes :id

  attribute :label do object.bill.label end
  attribute :price do object.bill.price end
  attribute :category do object.bill.category.nil? ? nil : object.bill.category.id end
  attribute :payer do object.payer.id end

  # payed attributes
  attributes :claim, :payedAt
end
