class BillSerializer < ActiveModel::Serializer
  attributes :id, :label, :price

  belongs_to :category, serializer: CategorySerializer
end
