require_relative "../services/bill_service.rb"

class PayedController < ApplicationController

  def index
    payed_all_joins = Payed.all.left_outer_joins(bill: [:category])
    render json: payed_all_joins
  end

  def show
    render json: Payed.find(params[:id]).left_outer_joins(bill: [:category])
  end

  def create
    payed = nil
    Bill.transaction do
      Payed.transaction do
        bill = BillService.new.create(bill_params.to_hash)
        payed = PayedService.new.create(bill, payed_params)
      end
    end

    render json: payed, include: '**'
  end

  def update
    payed = Payed.find(params[:id])
    Bill.transaction do
      Payed.transaction do
        BillService.new.update(payed.bill, bill_params.to_hash)
        PayedService.new.update(payed, payed.bill, payed_params)
      end
    end

    render json: payed, include: '**'
  end

  def destroy
    Payed.destroy(params[:id])
  end

  private

  def bill_params
    params
      .permit(:label, :price, :category)
  end

  def payed_params
    params
      .with_defaults({ payedAt: Time.now })
      .permit(:payer, :payedAt)
  end

end
