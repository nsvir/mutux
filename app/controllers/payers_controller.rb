class PayersController < ApplicationController
  def index
    render json: Payer.all
  end

  def update
    payer = Payer.find(params[:id])

    payer.update(payer_params)
    
    render json: payer
  end

  private 

  def payer_params
    params.permit(:name, :payRatio)
  end

end
