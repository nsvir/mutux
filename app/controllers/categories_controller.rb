class CategoriesController < ApplicationController

  def index
    render json: Category.all()
  end

  def show
    render json: Category.find(params[:id])
  end
  
  def create
    category = Category.create(category_params)
    render json: category
  end

  def update
    category = Category.find(params[:id])
    category.update(category_params)
    render json: category
  end

  def destroy
    Category.destroy(params[:id])    
  end  

  private 

  def category_params
    params.permit(:name)
  end

end
