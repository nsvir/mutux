# README

## Requirements

```bash
rbenv --version
git clone https://github.com/rbenv/ruby-build.git "$(rbenv root)"/plugins/ruby-build
git clone git://github.com/jf/rbenv-gemset.git "$(rbenv root)"plugins/rbenv-gemset
```

## Setup
```bash
rbenv install $(cat .ruby-version)
gem install bundle
bundle install
```
