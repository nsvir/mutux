Rails.application.routes.draw do
  scope '/api/v1' do
    resources :categories
    resources :payers
    scope '/bills' do
      resources :payed
    end
  end
end
